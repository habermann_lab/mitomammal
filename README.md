# mitoMammal: A Comprehensive Metabolic Model of Mammalian Mitochondria

This repository contains the mitoMammal metabolic model, a detailed representation of mammalian mitochondrial metabolism. The repository also includes accompanying Jupyter notebooks and datasets used in this work, which provide tools for model simulation, validation, and further exploration.

## Repository Contents

- `data/`: Contains all datasets used in this study.
- `6_mitoMammal_model.xml`: The core metabolic model in SBML format.
- `Chapman_etal_Table_S1a_tab.txt`: A text file describing reaction attributes of mitoMammal. This is Supplementary table 1 from the paper.
- `README.md`: The README file that describes this work.
- `efflux_method.py`: The E-Flux function used to constrain mitoMammal with the data.
- `proteomics_mouse_cardiac_vs_BAT.ipynb`: Jupyter notebook used to integrate proteomics cardiac and BAT mouse data in this paper.
- `transcriptomic_human_BAT.ipynb`: Jupyter notebook used to integrate the transcriptome from human BAT cell lines.

## Installation

To get started with the Jupyter notebooks and simulation tools, you will need to set up a Python environment with the necessary dependencies. We recommend using conda or pip for installation.

### Using Conda:

```bash
conda create --name mitomammal python=3.8
conda activate mitomammal
conda install jupyter numpy pandas matplotlib cobra
```

### Using Pip:

```bash
pip install jupyter numpy pandas matplotlib cobra
```

## Usage

Once the environment is set up, you can start Jupyter Notebook by running:

```bash
jupyter notebook
```

This will open a local server where you can explore the available notebooks in this repository.

### Key Notebooks:

- `proteomics_mouse_cardiac_vs_BAT.ipynb`: Jupyter notebook used to integrate proteomics cardiac and BAT mouse data in this paper.
- `transcriptomic_human_BAT.ipynb`: Jupyter notebook used to integrate the transcriptome from human BAT cell lines.

## Model Details

The mitoMammal model is a comprehensive and highly curated metabolic model that focuses on mitochondrial pathways relevant to energy production, reactive oxygen species, and key biosynthetic processes. It was developed from mitoCore, a human mitochondrial metabolic model. We amended mitoCore by adding mouse GPRs, correcting orthalogues and paralog genes and added missing reactions belonging to the DHODH reduction of Coenzyme Q within the electron transport chain.

## Usage

Clone the Jupyter notebooks to your working directory, and run each notebook line by line. The Jupyter notebooks have been annotated, but each notebook:

1. Imports necessary packages
2. Loads the mitoMammal metabolic model
3. Constrains the model using the E-Flux function and optimizes the objective function using FBA
4. Prints flux predictions
5. Plots import/export reactions

## Contributing

We welcome contributions to improve the mitoMammal model. If you find bugs or have suggestions for new features, please feel free to open an issue or submit a pull request. All contributors must adhere to the repository's contribution guidelines.

## License

This project is licensed under the MIT License - see the LICENSE file for details.

## Contact

For questions or collaborations, please contact the project lead or raise an issue on this repository.

## Citation

Please cite, if using this code:  MitoMAMMAL: a genome scale model of mammalian mitochondria predicts cardiac and BAT metabolism. Stephen P. Chapman, Theo Brunet, Arnaud Mourier, Bianca H. Habermann. bioRxiv 2024.07.26.605281; doi: https://doi.org/10.1101/2024.07.26.605281 
